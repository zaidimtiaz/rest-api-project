# Coding Challenge

## The task

Your task is to create a simple web service serving NHL regular season games. You need to create an CREATE/UPDATE/READ/DELETE(CRUD) endpoint/resource for games  and teams

Example of expected solution for a list of all games:

```bash
# Web service serving list of games
# http://localhost:<port>/games

curl -i http://localhost:5000/games
HTTP/1.0 200 OK
Content-Type: application/json; charset=utf-8
Content-Length: xx
Server: Werkzeug/1.0.1 Python/3.7.6
Date: Tue, 09 Feb 2021 12:15:39 GMT

[
    {
        "game_id": 2011030221,
        "season": 20112012,
        .....
    },
    {
        "game_id": 20112012,
        "season": 20112012,
        ....
    },
    ...
]
```

**Setup:**

The repository has a skeleton Flask app for convenience.

-   Use correct [Python version](.python-version)
-   Initialize development environment `source ./init-project.sh`
-   Start the development server `make run`

**Other remarks:**

-   Since the data is historical, you don't need to worry about it being dynamically updated in the future for this service
-   you can either create a database and load all data or just create a dictionary of a selected data, just like in the class excercises

## Data description

The dataset consist of two files containing match results and team meta data. Datasets haven't been cleaned up so they contain columns you won't need to complete this task.

### [game_data.csv](data/game_data.csv)

Contains match results for all NHL regular season and play off games from seasons 2011-2012 to 2018-2019.

| Key                    | Description                                                        |
| ---------------------- | ------------------------------------------------------------------ |
| game_id                | Game key field as assigned by the NHL                              |
| season                 | Season identifier                                                  |
| type                   | R=Regular Season, P=Playoff                                        |
| date_time              | Match day                                                          |
| date_time_GMT          | Match day and time in GMT                                          |
| away_team_id           | Away team identifier                                               |
| home_team_id           | Home team identifier                                               |
| away_goals             | Away team goals                                                    |
| home_goals             | Home team goals                                                    |
| outcome                | REG=regular time, OT=overtime, SO=shootouts                        |
| home_rink_side_start   | Indicates the direction of play relative to the Time/Score keepers |
| venue                  |                                                                    |
| venue_link             |                                                                    |
| venue_time_zone_id     |                                                                    |
| venue_time_zone_offset |                                                                    |
| venue_time_zone_offset |                                                                    |
| venue_time_zone_tz     |                                                                    |

### [team_info.csv](data/team_info.csv)

Contains team metadata information.

| Key          | Description            |
| ------------ | ---------------------- |
| team_id      | Team identifier        |
| franchiseId  | Franchise identifier   |
| shortName    | Team short name        |
| teamName     | Team name              |
| abbreviation | Team name abbreviation |
| link         |                        |


## Evaluation criteria

Your solution is evaluated based on the following criteria:

-   Individual git commits

    -   Readable commit messages
    -   Commit size

-   Python usage

    -   Readability
    -   Performance
    -   Packages used
    -   Code quality (data stucturs etc.)
    -   Typings
    -   Following linting rules

-   Tests
    -   Tested http endpoint
        -   Success cases (200)
        -   Error cases (eg. invalid season id)
    -   Unit tests where applicable
