from flask import Flask, jsonify, request, render_template
from flask_restful import Resource, Api,

app = Flask(__name__)

api = Api(app)

games = [{
    "game_id": 2011030221,
    "season": 20112012
},
{
    "game_id": 2011030221,
    "season": 20112012
}]

# @app.route('/')
# def hello_world():
#     return 'Hello, World!'


#
# #get /games
# @app.route('/games')
# def get_games():
#   return jsonify({'games': games})




class GameList(Resource):
    def get(self):
        return {'games': games}


api.add_resource(GameList, '/games')
