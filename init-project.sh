#!/bin/bash

python3 -m venv venv
# for Windows
# ./venv/Scripts/activate.bat
# for macos or linux
source venv/bin/activate

pip install --upgrade pip
pip install -r requirements.txt